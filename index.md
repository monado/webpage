---
title: "Monado - Developer Site"
layout: main
---

# Monado - XR Runtime (XRT)
{:.no_toc}

* TOC
{:toc}

## What is Monado?

Monado is an open source XR runtime delivering immersive experiences such as VR
and AR on mobile, PC/desktop, and other devices. Monado aims to be a complete
and conformant implementation of the OpenXR API made by Khronos. The project
is currently being developed for GNU/Linux, Android and Windows.

Most of the included VR hardware drivers are limited to Linux at this time.

On Android Monado comes with support for the built in IMU of most smartphones to
enable 3DoF experiences, similar to Google Cardboard.

On Windows, mostly simulated HMD and controller drivers are usable. Direct Mode
access for existing HMDs when not using NDA APIs remains a major blocker.

## Important links

* Source code: <https://gitlab.freedesktop.org/monado/monado>
* Matrix space: <https://matrix.to/#/#monado:matrix.org>
* Discord server: <https://discord.gg/8RkJgRJ>
* IRC channel: [#monado](https://webchat.oftc.net/?channels=monado) on
  [OFTC](https://oftc.net/)

## Highlights

* [Officially an OpenXR conformant implementation](https://www.collabora.com/news-and-blog/news-and-events/monado-2100-officially-conformant-openxr-implementation.html) of the [OpenXR API](https://www.khronos.org/openxr)
  * Supports Vulkan, OpenGL & ES, D3D11 (Windows only), D3D12 (Windows only), and Headless applications
  * Full support for Local, Stage, Local Floor, and Action space relations
  * Action based input
  * OpenXR Extensions:
    * XR_KHR_convert_timespec_time
    * XR_KHR_opengl_enable, XR_KHR_opengl_es_enable, XR_KHR_vulkan_enable, XR_KHR_vulkan_enable2
    * XR_KHR_composition_layer_depth, XR_KHR_composition_layer_cylinder, XR_KHR_composition_layer_equirect, XR_KHR_composition_layer_equirect2
    * XR_EXT_hand_tracking - with Index controllers on vive and libsurvive drivers and Leap Motion with Ultraleap v2
    * XR_EXTX_overlay - [Multi-application support](https://www.collabora.com/news-and-blog/news-and-events/monado-multi-application-support-with-xr-extx-overlay.html)
    * XR_MND_headless - XrSession without graphical output and compositor
    * XR_KHR_swapchain_usage_input_attachment_bit / XR_MND_swapchain_usage_input_attachment_bit
    * XR_MNDX_egl_enable - Alternative to XR_KHR_opengl_enable and XR_KHR_opengl_es_enable on Desktop
    * XR_KHR_android_create_instance, XR_KHR_loader_init and XR_KHR_loader_init_android - android specific
    * And many more. See the EXTENSIONS list in [this script](https://gitlab.freedesktop.org/monado/monado/-/blob/main/scripts/generate_oxr_ext_support.py) for the full list of extensions Monado can currently support
* 6DoF tracking
  * PSVR and PS Move controllers: Outside-in tracking framework with stereo cameras such as PS4 camera
    * Work in progress on outside-in tracking with mono cameras such as consumer webcams
  * Inside-out (VIO/SLAM) tracking using [an improved basalt fork](https://gitlab.freedesktop.org/mateosss/basalt/) with all supported stereo or more cameras such as Windows Mixed Reality, Oculus Rift S, Valve Index
* Video stream and filter framework for tracking components
* [Initial Android support](https://www.collabora.com/news-and-blog/news-and-events/monado-update-passing-conformance-android-support-and-more.html)
  * Cardboard viewer
* XR Compositor
  * [Direct mode on AMD, NVidia and Intel GPUs]({% link direct-mode.md %})
  * Mesh based distortion with generators for various supported HMDs such as Vive/Index parameters, Windows Mixed Reality, OpenHMD's Panotools
  * Supports multiple simultaneous projection layers and quad layers
* Driver framework allowing easy integration of existing drivers
  * Out of the box support for multiple XR devices with open source drivers

## Supported Hardware

These are the XR devices that are natively supported with open source drivers in Monado

| Device | Tracking | OS  | Notes |
|:------:|:--------:|:---:|:-----:|
| OSVR HDK 1.x, 2.x | 3DoF | Linux | Requires workaround on AMD GPUs[^yuv_edid]. Firmware fix available[^hdk_edid_fix]. No distortion correction, [distortion correction WIP available](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2198]) |
| HTC Vive, HTC Vive Pro, Valve Index ("vive driver)| 3DoF, or 6DoF with Monado Basalt (Valve Index only) | Linux | |
| North Star | 6DoF with Intel Realsense T265 on-device tracking, 6DoF with Monado Basalt | Linux | Both 3D and 2D/Polynomial distortion correction supported. |
| Oculus Rift S | 6Dof with Monado Basalt | Linux | Controllers are 3DoF only [WIP 6DoF controller tracking branch available](https://gitlab.freedesktop.org/thaytan/monado/-/commits/dev-constellation-controller-tracking/). Imperfect distortion correction. |
| PSVR | 3DoF, or [6DoF with PS4 camera or generic stereo camera]({% link positional-tracking-psmove.md %}) | Linux | distortion correction is WIP. Requires workaround on AMD GPUs[^yuv_edid]. |
| Playstation Move | 3DoF, or [6DoF with PS4 or generic stereo camera]({% link positional-tracking-psmove.md %}) | Linux | rotational drift correction is WIP |
| Hydra Controller | 6DoF | Linux | 6DoF tracking on device in firmware |
| Daydream Controller | 3DoF | Linux | |
| DIY arduino controller | 3DoF | Linux | |
| Windows MR | 3DoF, or 6DoF with Monado Basalt | Linux, [Windows WIP, no direct mode](https://gitlab.freedesktop.org/monado/monado/-/issues/466#note_2733388) | Tested: HP Reverb, HP Reverb G2, Samsung Odyssey, Lenovo Explorer, others: *maybe*. Controllers are 3DoF only [WIP 6DoF controller tracking branch available](https://gitlab.freedesktop.org/thaytan/monado/-/commits/dev-constellation-controller-tracking/) |
| Xreal Air | 3DoF | Linux | |
| Xreal Air 2 | 3DoF | Linux | |
| Xreal Air 2 Pro | 3DoF | Linux | |


\* [Monado Basalt](https://gitlab.freedesktop.org/mateosss/basalt/): _Experimental 6DoF tracking support with external SLAM/VIO systems._

Monado also leverages the open source drivers developed by the OpenHMD community
for further HMD support.

See the [OpenHMD support matrix](http://www.openhmd.net/index.php/devices/)
for a list of devices supported through OpenHMD. Most notably, OpenHMD supports
the Oculus Rift CV1 and Nolo CV1.

The Direct-Tek WVR2 / VR-Tek Windows VR Glasses with the 2560x1440 resolution
supported through OpenHMD requires a workaround on AMD GPUs[^yuv_edid].

XR devices that are supported by wrapping 3rd party open source drivers

| Device | 3rd party driver | Tracking | Additional Notes |
|:------:|:----------------:|:--------:|:--------:|:-----:|
| HTC Vive, HTC Vive Pro, Valve Index | [libsurvive](https://github.com/collabora/libsurvive) | 6DoF with Lighthouse 1.0 and 2.0 | [survive]({% link libsurvive.md %}) driver must be enabled at build time |
| T265 realsense | [librealsense](https://github.com/IntelRealSense/librealsense) | 6Dof with proprietary on-device SLAM tracking |

Proprietary 3rd party drivers can also be integrated. Monado currently wraps these

| Device | proprietary 3rd party driver | Tracking | Additional Notes |
|:------:|:----------------------------:|:--------:|:----------------:|
| Leap Motion Controller | Ultraleap v2 | 6DoF Hand Tracking |  |
| HTC Vive, HTC Vive Pro, Valve Index | steamvr_lh | 6DoF with Lighthouse 1.0 and 2.0 | Wraps SteamVR's proprietary lighthouse tracking |

See also the XR Devices table in the [LVRA Wiki](https://lvra.gitlab.io/docs/hardware/#xr-devices) that also includes WIP and out of tree drivers.

### So what does that mean?

For end users it means Monado can be used to run OpenXR games and
applications like Blender on any of the supported hardware.

For developers it means you can start developing software for OpenXR
with the ability to debug and inspect the source code of the entire XR
software stack from your application to the HMD driver.

Monado transparently takes care of direct mode and distortion correction
without developers having to write a single line of X11 code.

## Getting Started with Monado

* [Setup Monado, libsurvive and OpenComposite for the Valve Index]({% link valve-index-setup.md %})
* [Getting Started installing and running applications with Monado]({% link getting-started.md %})
* [The OpenXR 1.0 specification](https://www.khronos.org/registry/OpenXR/specs/1.0/html/xrspec.html)
* [List of Open Source applications and examples]({% link openxr-resources.md %})
* [Tested Multi-GPU configurations (e.g. Optimus)]({% link multi-gpu.md %})
* [For hacking on Monado (e.g. implementing OpenXR extensions), take a look at the online doxygen documentation](https://monado.pages.freedesktop.org/monado/)
* [Use Monado's hardware drivers in SteamVR]({% link steamvr.md %})

## Other Information

* [Khronos' Monado-Improvements project 2023]({% link monado-improvements-project-2023.md %})

## Code of Conduct

We follow the standard freedesktop.org code of conduct, available at
<https://www.freedesktop.org/wiki/CodeOfConduct/>, which is based on the
[Contributor Covenant](https://www.contributor-covenant.org).

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting:

* First-line project contacts: see main repo README <https://gitlab.freedesktop.org/monado/monado#contributing-code-of-conduct>
* freedesktop.org contacts: see most recent list at
  <https://www.freedesktop.org/wiki/CodeOfConduct/>

## Contributing & Repos

The main repository is <https://gitlab.freedesktop.org/monado/monado> and has
documentation on how to get started and contribute. Please refer to the
`CONTRIBUTING.md` file in the repository for details.

Contributions to the information on this website itself are welcome at
<https://gitlab.freedesktop.org/monado/webpage>

## Contact

For other questions and just hanging out you can find us here:

* [Discord](https://discord.gg/8RkJgRJ) server.
* [#monado](https://webchat.oftc.net/?channels=monado) on [OFTC](https://oftc.net/).

# Footnotes
{:.no_toc}

[^yuv_edid]: An issue with the EDID results in wrong colors or black screen on AMD GPUS. See [EDID override]({% link edid-override.md %}) for details and workarounds.
[^hdk_edid_fix]: Firmware was fixed [in this PR](https://github.com/OSVR/OSVR-HDK-MCU-Firmware/pull/31). Builds can be found [here](https://dev.azure.com/osvr/OSVR-HDK-MCU-Firmware/_build/results?buildId=9&view=artifacts&type=publishedArtifacts) - Choose a successful pipeline (green checkmark), choose "1 published", in the 3 dots menu choose "Download Artifacts".
